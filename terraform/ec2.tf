resource "aws_instance" "master" {
  ami             = "ami-00874d747dde814fa"
  instance_type   = var.instance
  private_ip      = "172.31.55.106"
  user_data       = file("postinstall.sh")
  security_groups = ["${aws_security_group.allow_http_ssh.name}"]

  tags = {
    Owner = var.owner
    Name  = "master"
  }
}

resource "aws_instance" "worker1" {
  ami             = "ami-00874d747dde814fa"
  instance_type   = var.instance
  user_data       = file("postinstall.sh")
  security_groups = ["${aws_security_group.allow_http_ssh.name}"]

  tags = {
    Owner = var.owner
    Name  = "worker1"
  }
}

resource "aws_instance" "worker2" {
  ami             = "ami-00874d747dde814fa"
  instance_type   = var.instance
  user_data       = file("postinstall.sh")
  security_groups = ["${aws_security_group.allow_http_ssh.name}"]

  tags = {
    Owner = var.owner
    Name  = "worker2"
  }
}

resource "aws_instance" "worker3" {
  ami             = "ami-00874d747dde814fa"
  instance_type   = var.instance
  user_data       = file("postinstall.sh")
  security_groups = ["${aws_security_group.allow_http_ssh.name}"]

  tags = {
    Owner = var.owner
    Name  = "worker3"
  }
}

